import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  perPage;

  constructor(private itemService: ItemService) { }

  // получим значение perPage из сервиса
  ngOnInit() {
    this.perPage = this.itemService.perPage;
  }

  // установим наше значение perPage в сервис
  onChange() {
    this.itemService.setPerPage(+this.perPage);
  }

}
