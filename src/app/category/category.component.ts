import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  items = [];

  constructor(private itemService: ItemService) { }

  // при инициализации подпишемся на данные из ItemService
  ngOnInit() {
    this.itemService.getItems().subscribe(data => {
      this.items = data.results;
    });
  }

}
