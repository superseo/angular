import { Http } from '@angular/Http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class ItemService {

  // значение по умолчанию
  perPage = 4;
  constructor(private http: Http) { }

  // геттер элементов
  // http объект возвращает http rxjs стрим, на который нужно подписаться
  getItems() {
    return this.http.get(`https://randomuser.me/api?results=${this.perPage}`).map(response => response.json());
  }

  // сеттер нового значения perPage
  setPerPage(perPage) {
    this.perPage = perPage;
  }

}
