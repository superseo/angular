import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  // этим декоратором говорим что это входные данные в переменную item
  @Input() item;

  constructor() { }

  ngOnInit() {
  }

}
